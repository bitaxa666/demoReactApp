package com.example.demoReactApp.repository;

import com.example.demoReactApp.employee.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by user on 3/6/18.
 */
public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Long> {

}
